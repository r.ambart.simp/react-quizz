import { useState } from "react";
import { useEffect } from "react";

const abc = useEffect((async_f) => {
    var [response, setresponse] = useState(null);
    var [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    const endLoading = () => {
        setIsLoading(false);
    }
    async const get_response = () => {
        let rep = null;
        let err = null;
        try {
            rep = await async_f();
        }
        catch(error) {
            err = error;
        }
        setResponse(rep);
        setError(err);
        setIsLoading(false);
    }
    get_response();
    return ({response, error, isLoading});
});