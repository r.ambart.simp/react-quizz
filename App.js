import React from "react";
import { StyleSheet } from "react-native";
import "react-native-gesture-handler";
import Navigator from "./src/navigation";
import {firebaseConfig} from "./src/constants/FSconfig"
import firebase from "firebase/app";
import UserContextProvider from "./src/contexts/userContext"


export default function App() {
  if (!firebase.apps.length) {
    console.log('Connected with Firebase')
    firebase.initializeApp(firebaseConfig)
  }

  return <UserContextProvider><Navigator /></UserContextProvider>;
}
