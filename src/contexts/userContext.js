import React, { createContext, useState } from "react";

export const UserContext = createContext({});

const { Provider } = UserContext;

const UserContextProvider = ({ children }) => {
  const [userName, setUserName] = useState("");

  return <Provider value={{ userName, setUserName }}>{children}</Provider>;
};

export default UserContextProvider;