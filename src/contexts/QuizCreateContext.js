import React, { createContext, useState } from "react";

export const QuizCreateContext = createContext({});

const { Provider } = QuizCreateContext;

const QuizCreateContextProvider = ({ children }) => {
  const [name, setName] = useState("");
  const [nbQuestion, setNbQuestion] = useState(0);
  const [questions, setQuestions] = useState([0]);

  return <Provider value={{ name, setName, nbQuestion, setNbQuestion, questions, setQuestions }}>{children}</Provider>;
};

export default QuizCreateContextProvider;