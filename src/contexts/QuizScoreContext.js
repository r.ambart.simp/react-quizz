import React, { createContext, useState } from "react";

export const GameContext = createContext({});

const { Provider } = GameContext;

const GameContextProvider = ({ children }) => {
  const [score, setScore] = useState(0);
  const [questionNum, setQuestionNum] = useState(0);
  const [quiz, setQuiz] = useState({});

  return <Provider value={{ score, setScore, questionNum, setQuestionNum, quiz, setQuiz}}>{children}</Provider>;
};

export default GameContextProvider;