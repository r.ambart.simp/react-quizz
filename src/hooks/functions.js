import Quizz from "../interface/Quizz";
import * as firebase from "firebase/app";
import "firebase/firestore"



export const addQuiz = async ( quiz) => {
    const db = firebase.default.firestore();
    return await db.collection()
        .set(quiz)
        .then(() => {
            return null;
        });
}

export const getQuiz = async (quizName) => {
    const db = firebase.default.firestore();
    const res = await db.collection('quiz')
        .doc(quizName)
        .get();
        
    if (!res.exists) {
        throw new Error();
    }
    return res;
}

export const getAllQuiz = async () => {
    const db = firebase.default.firestore();
    const tab = [];
   const rep = await db.collection("quiz")
    .get()
    .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            // doc.data() is never undefined for query doc snapshots
            tab.push(doc.data);
        });
        return tab;
    })
    .catch((error) => {
        console.log("Error getting documents: ", error);
    });
}
