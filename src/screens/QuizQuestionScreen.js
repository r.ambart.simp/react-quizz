import { NavigationContext } from "@react-navigation/core";
import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { routes } from "../constants/routes";
import { GameContext } from "../contexts/QuizScoreContext";

function getQuizAnswer(context, x)
{
  return (context.quiz.questions[context.questionNum].answers[x].content);
}

const QuizQuestionScreen = ({navigation}) => {
  const context = React.useContext(GameContext);

  const getUserAnswer = (id) => {
    if (context.quiz.questions[context.questionNum].answers[id].isCorrect == true) {
      alert("Bonne réponse");
      context.setScore(context.score + 1);
    }
    else alert("Mauvaise réponse");
    if (context.questionNum == 4) navigation.navigate(routes.QUIZ_RESULTS);
    else
      context.setQuestionNum(context.questionNum + 1);
  }
  return (
    <View style={styles.container}>
      <Text>Question {context.questionNum+1}/5</Text>
      <Text>{context.quiz.questions[context.questionNum].question}</Text>
      <br />
      <Button title="Réponse 1" onPress={() => {getUserAnswer(0)}}/>
      <Text>{getQuizAnswer(context, 0)}</Text>
      <Button title="Réponse 2" onPress={() => {getUserAnswer(1)}}/>
      <Text>{getQuizAnswer(context, 1)}</Text>
      <Button title="Réponse 3" onPress={() => {getUserAnswer(2)}}/>
      <Text>{getQuizAnswer(context, 2)}</Text>
      <Button title="Réponse 4" onPress={() => {getUserAnswer(3)}}/>
      <Text>{getQuizAnswer(context, 3)}</Text>
    </View>
  );
};

export default QuizQuestionScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});