import React from "react";
import { useContext } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { routes } from "../constants/routes";
import { QuizCreateContext } from "../contexts/QuizCreateContext";

const AddQuizQuestionScreen = ({navigation}) => {
  const [question, setQuestion] = React.useState("");
  const [respNum, setRespNum] = React.useState(0);
  const [answerOne, setAnswerOne] = React.useState("");
  const [answerTwo, setAnswerTwo] = React.useState("");
  const [answerThree, setAnswerThree] = React.useState("");
  const [answerFour, setAnswerFour] = React.useState("");
  const context = useContext(QuizCreateContext);

  const sendQuestion = () => {
    var q = {question,
            respNum,
            answers: [answerOne, answerTwo, answerThree, answerFour]};
    if (context.nbQuestion != 0)
      context.setQuestions([...context.questions, q]);
    else context.setQuestions([q]);
    setQuestion("");
    setRespNum(0);
    setAnswerOne("");
    setAnswerTwo("");
    setAnswerThree("");
    setAnswerFour("");
    if (context.nbQuestion == 4) {
      // Ajoute le quiz au firebase
        alert("Le quiz a bien été enregistré");
        navigation.navigate(routes.HOME);
    }
    else 
    {
      context.setNbQuestion(context.nbQuestion+1);
      navigation.navigate(routes.ADD_QUIZ_QUESTION);
    }
  }

  return (
    <View style={styles.container}>
      Question: <TextInput placeholder="Question" value={question} onChangeText={setQuestion}/>
      <div><Button title="+" onPress={() => {setRespNum(0)}} /><TextInput placeholder="Reponse 1" value={answerOne} onChangeText={setAnswerOne}/>{respNum == 0 ? "V" : ""}</div>
      <div><Button title="+" onPress={() => {setRespNum(1)}} /><TextInput placeholder="Reponse 2" value={answerTwo} onChangeText={setAnswerTwo}/>{respNum == 1 ? "V" : ""}</div>
      <div><Button title="+" onPress={() => {setRespNum(2)}} /><TextInput placeholder="Reponse 3" value={answerThree} onChangeText={setAnswerThree}/>{respNum == 2 ? "V" : ""}</div>
      <div><Button title="+" onPress={() => {setRespNum(3)}} /><TextInput placeholder="Reponse 4" value={answerFour} onChangeText={setAnswerFour}/>{respNum == 3 ? "V" : ""}</div>
      <Button title="Valider" onPress={sendQuestion}/>
    </View>
  );
};

export default AddQuizQuestionScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
