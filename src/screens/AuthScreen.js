import { getAllQuiz } from "../hooks/functions";
import React, { useContext } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { UserContext } from "../contexts/userContext";
import { routes } from "../constants/routes";

const AuthScreen = ({navigation}) => {
  const [text, onChangeText] = React.useState("");
  const context = useContext(UserContext);

  const changeUsername= () => {
    if (text != "") {
      context.setUserName(text);
      navigation.navigate(routes.HOME);
    }
  }
  return (
    <View style={styles.container}>
      <Text>Hello Auth !</Text>
      <TextInput placeholder="Username" value={text} onChangeText={onChangeText}></TextInput>
      <br />
      <Button title="Login" onPress={changeUsername} />
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
    </View>
  );
}

export default AuthScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
