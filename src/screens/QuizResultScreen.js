import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { routes } from "../constants/routes";
import { GameContext } from "../contexts/QuizScoreContext";

const QuizResultScreen = ({navigation}) => {
  const context = React.useContext(GameContext);

  return (
    <View style={styles.container}>
      <Text>Votre score est de {context.score} / 5</Text>
      <Button title="HOME" onPress={() => {navigation.navigate(routes.HOME)}}/>
    </View>
  );
};

export default QuizResultScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
