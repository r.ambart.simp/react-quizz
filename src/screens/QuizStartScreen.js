import React from "react";
import { StyleSheet, Text, View, Button} from "react-native";
import { routes } from "../constants/routes";
import { GameContext } from "../contexts/QuizScoreContext";

const QuizStartScreen = ({navigation}) => {
  const context = React.useContext(GameContext);

  return (
    <View style={styles.container}>
      <Text>Hello QuizStart !</Text>
      <Button title="Start quiz" onPress={()=> {context.setQuiz({questions:[{question:"a", answers:[{content:"b", isCorrect: true}, {content:"c", isCorrect: false}, {content:"d", isCorrect: false}, {content:"e", isCorrect: false}]}, {question:"a", answers:[{content:"c", isCorrect: false}, {content:"b", isCorrect: true}, {content:"d", isCorrect: false}, {content:"e", isCorrect: false}]}, {question:"a", answers:[{content:"d", isCorrect: false}, {content:"c", isCorrect: false}, {content:"b", isCorrect: true}, {content:"e", isCorrect: false}]}, {question:"b", answers:[{content:"e", isCorrect: false}, {content:"c", isCorrect: false}, {content:"d", isCorrect: false}, {content:"b", isCorrect: true}]}, {question:"a", answers:[{content:"b", isCorrect: true}, {content:"c", isCorrect: false}, {content:"d", isCorrect: false}, {content:"e", isCorrect: false}]}]}); navigation.navigate(routes.QUIZ_QUESTION)}} />
    </View>
  );
};

export default QuizStartScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
