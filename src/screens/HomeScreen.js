import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { UserContext } from "../contexts/userContext";
import { routes } from "../constants/routes";;

const HomeScreen = ({navigation}) => {
  const context = React.useContext(UserContext);

  return (
    <View style={styles.container}>
      <Text>Bievenue {context.userName}!</Text>
      <Text>Que voulez vous faire ?</Text>
      <Button title="Créer un quiz" onPress={()=>{navigation.navigate(routes.ADD_QUIZ_NAME)}} />
      <Button title="Faire un quiz" onPress={()=>{navigation.navigate(routes.QUIZ_START)}} />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
