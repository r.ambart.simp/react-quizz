import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { routes } from "../constants/routes";
import { QuizCreateContext } from "../contexts/QuizCreateContext";

const AddQuizNameScreen = ({navigation}) => {
  const [text, onChangeText] = React.useState("");
  const context = React.useContext(QuizCreateContext);

  const submitQuizName = () => {
    if (text != "") {
      context.setName(text);
      navigation.navigate(routes.ADD_QUIZ_QUESTION);
    }
  }

  return (
    <View style={styles.container}>
      <Text>Veuillez donnez un nom a votre quiz:</Text>
      <TextInput placeHolder="Nom du quiz" value={text} onChangeText={onChangeText} />
      <br />
      <Button title="Validez" onPress={submitQuizName}/>
    </View>
  );
};

export default AddQuizNameScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});