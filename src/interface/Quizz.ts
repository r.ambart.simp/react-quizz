export interface IAnswer {
    content: string;
    isCorrect: boolean;
}

export interface IQuestion {
    question: string;
    answers: IAnswer[];
}

export default interface IQuiz {
    author: string;
    questions: IQuestion[];
}